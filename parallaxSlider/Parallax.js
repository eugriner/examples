// Utilizei como classes mesmo, mas eram nomes únicos. 
// Acho que da sim para utilizar com mesmo nome. CREIO QUE SIM 
// Mas vamos lá, acho que da para entender legal.. 	

// A função é chamada quando há o scroll da janela (o mause no caso, ou setas)
$(window).scroll(function() {

	// Vai pegar o valor de cada ponto do scroll ao descer a janela..
	var wScroll = $(this).scrollTop();

	//PARA FAZER A TRANSLAÇÃO VERTICAL: 
	$('.logo-header').css({
		'transform' : 'translate(0px, ' + wScroll / 9 + '%)'
	});

	//CASO QUEIRA FAZER HORIZONTL:
	$('.bg-header').css({
	    'transform' : 'translate(0px, ' + wScroll / 6 + '%)'
	});
	
	
	// !!!! PARA ALTERAR O SENTIDO, VOCE DEVE MUDAR O SINAL
	// OU ADD UM SINAL DE - FOOOOORAAAAA DAS ASPAS '' 
	
	//EX DE PARALLAX COM SENTIDO CONTRÁRIO
	$('.notebook-header').css({
	    // O sinal de menos está depois da virgula.
		'transform' : 'translate(0px, -' + wScroll / 20 + '%)'
	});

});